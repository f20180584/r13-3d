
import sys
import os
import copy
import time as time_module
import dolfin as df
import ufl
import numpy as np
import tensoroperations as to
from meshes import H5Mesh

v = df.as_vector([1.0, 2.0, 3.0])
a=v[0]
#print(a)

v1=df.as_vector([1.0, 0, 0])

a=df.Vector()
m=a.norm('l2')
print(m)
v2=df.as_vector([0,1,0])
mesh = H5Mesh('annulus5.h5')
mesh1=mesh.mesh
n_vec = df.FacetNormal(mesh1)
#t1=df.cross(n_vec,v)#I am not sure whther we have to convert that to as_vector
t1=df.conditional(df.gt(abs(n_vec[0]),0),df.cross(n_vec,v2),df.cross(n_vec,v1))
t2=df.cross(n_vec,t1)
#print(n_vec[0])
#df.list_linear_solver_methods()
#df.list_krylov_solver_preconditioners()
#df.list_linear_algebra_backends()
#df.list_linear_solver_methods()
#df.list_lu_solver_methods()
#problem=df.LinearVariationalProblem(0,0,0)
#solver = df.LinearVariationalSolver(problem)
#print(solver.default_parameters())
#df.info(df.LinearVariationalSolver.default_parameters(), 1)
#print(solver.parameters["relative_tolerance"])
#print(solver.parameters["maximum_iterations"])
#print(solver.parameters["monitor_convergence"])
#prm = parameters.krylov_solver