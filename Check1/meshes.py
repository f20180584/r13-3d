"""
Module to store the mesh classes.

Currently, the only mesh format is h5.
"""

import os
import dolfin as df


class H5Mesh:
    """
    Mesh class.

    Raises
    ------
    Exception
        File not found

    Examples
    --------
    >>> mesh = H5Mesh("non_existing_mesh.h5")
    Traceback (most recent call last):
    ...
    Exception: non_existing_mesh.h5 not found

    """

    def __init__(self, h5_file):
        """
        Construct the object.

        This includes:

            (#) Read mesh
            (#) Read subdomains
            (#) Read boudnaries

        """
        if not os.path.isfile(h5_file):
            raise Exception(f"{h5_file} not found")

        self.mesh = df.Mesh()

        hdf = df.HDF5File(self.mesh.mpi_comm(), h5_file, "r") #this creates a HDF5 class instance hdf and sets it to read mode and uses the mesh object of H5 class that we created rn

        hdf.read(self.mesh, "/mesh", False)#this reads the mesh onto H5.mesh property. It reads it as a vector and not a mesh. Uses the 1st of the overloaded fucntions in the documentation."/mesh refrencing the mesh under the H% file"
        dim = self.mesh.topology().dim()#gives dimension-pretty clear

        self.subdomains = df.MeshFunction("size_t", self.mesh, dim)#this defines the property of H5 class - subdomains as a meshfunction on self.mesh. Am unable to figure out size_t tho
        hdf.read(self.subdomains, "/subdomains")#this uses the hdf object again to read the subdomains from H5 file onto self.subdomains."/subdomains' is referncing the h5 file

        self.boundaries = df.MeshFunction("size_t", self.mesh, dim - 1)
        hdf.read(self.boundaries, "/boundaries")
