"""
Converter from msh-format to a mesh in h5-format.

Usage:

.. code-block:: bash

    Usage: python3 <path_to_mshToH5.py> <msh_file> <h5_file>
    E.g.:  python3 mshToH5.py lid lid5
"""

import os
import sys
import dolfin as df
ccd
def msh_to_h5():
    "Convert given geo-file to a h5-mesh."

    msh_input_file = sys.argv[1]
    h5_output_file = sys.argv[2]

    # Convert msh-mesh to xml-mesh
    os.system("dolfin-convert {0}.msh {0}.xml".format(msh_input_file))

    # Read xml-mesh
    mesh = df.Mesh("{}.xml".format(msh_input_file))
    subdomains = df.MeshFunction(
        "size_t", mesh, "{}_physical_region.xml".format(msh_input_file))
    boundaries = df.MeshFunction(
        "size_t", mesh, "{}_facet_region.xml".format(msh_input_file))

    # Delete xml-mesh
    os.remove("{}.xml".format(msh_input_file))
    os.remove("{}_physical_region.xml".format(msh_input_file))
    os.remove("{}_facet_region.xml".format(msh_input_file))

    # Write h5-mesh
    file = df.HDF5File(mesh.mpi_comm(), "{}.h5".format(h5_output_file), "w")
    file.write(mesh, "/mesh")
    file.write(subdomains, "/subdomains")
    file.write(boundaries, "/boundaries")


if __name__ == '__main__':
    msh_to_h5()